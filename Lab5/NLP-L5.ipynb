{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-danger\">\n",
    "**Due date:** 2018-02-23\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab 5: Semantic analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A **word embedding** represents words as vectors in a high-dimensional vector space. In this lab you will train word embeddings on the English Wikipedia via truncated singular value decomposition of the co-occurrence matrix.\n",
    "\n",
    "Start by loading the Python module for this lab:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nlp5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data for this lab is the text of the [Simple English Wikipedia](https://simple.wikipedia.org/wiki/Main_Page). We have excluded articles shorter than 50&nbsp;words, as well as certain meta-articles. The remaining articles were pre-processed by removing non-textual elements, sentence splitting, and tokenisation. The result is a text file containing 2.4M sentences, spanning 23M tokens.\n",
    "\n",
    "<div class=\"alert alert-warning\">\n",
    "Note that the data file is quite big. It is therefore very important to think about efficiency in this lab!\n",
    "</div>\n",
    "\n",
    "Because the data file is so big, we have compressed it using [bz2](https://en.wikipedia.org/wiki/Bzip2), which can be processed sequentially without completely decompressing the file. This functionality is provided by the `bz2` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import bz2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The (uncompressed) text contains one sentence per line, with individual tokens separated by spaces. To loop over the tokens, you can use the following generator function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def tokens(source):\n",
    "    for sentence in source:\n",
    "        for token in sentence.split():\n",
    "            yield token"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next code cell shows you how you can open the compressed data file and print the number of tokens in the text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "22963492\n"
     ]
    }
   ],
   "source": [
    "with bz2.open('/home/TDDE09/labs/l5/data/simplewiki.txt.bz2') as source:\n",
    "    print(sum(1 for t in tokens(source)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the use of the generator function to obtain the tokens is essential here &ndash; returning the tokens as a list would require a lot of memory. If you have not worked with generators and iterators before, now is a good time to read up on them. [More information about generators](https://wiki.python.org/moin/Generators)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build the vocabulary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your first task in this lab is to build the vocabulary of the word embedding that you are about to construct."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"panel panel-primary\">\n",
    "<div class=\"panel-heading\">Problem 1</div>\n",
    "<div class=\"panel-body\">\n",
    "Write code that builds the vocabulary of the word embedding. Represent the vocabulary as a dictionary that maps words to a contiguous range of integer ids. Ignore words that occur less than 100&nbsp;times.\n",
    "</div>\n",
    "</div>\n",
    "\n",
    "To solve this problem, complete the skeleton code in the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_vocab(source, threshold=100):\n",
    "    \n",
    "    vocabulary = {}\n",
    "    for next_word in nlp5.tokens(source):\n",
    "        vocabulary[next_word] = vocabulary.get(next_word, 0) + 1\n",
    "    v_with_threshold = {k: v for k, v in vocabulary.items() if v >= threshold}\n",
    "    i = 0\n",
    "    for word in v_with_threshold:\n",
    "        v_with_threshold[word] = i\n",
    "        i += 1\n",
    "        \n",
    "    return v_with_threshold\n",
    "    #return nlp5.make_vocab(source, threshold)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to help you test your implementation, we provide a smaller data file with the first 1M tokens from the full data. The code in the next cell builds the word-to-index mapping for this file and prints the size of the vocabulary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1256\n"
     ]
    }
   ],
   "source": [
    "with bz2.open('/home/TDDE09/labs/l5/data/simplewiki-small.txt.bz2') as source:\n",
    "    small_vocab = make_vocab(source)\n",
    "    print(len(small_vocab))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you are confident that your implementation is correct, you can run it on the full data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 158,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "14887\n"
     ]
    }
   ],
   "source": [
    "with bz2.open('/home/TDDE09/labs/l5/data/simplewiki.txt.bz2') as source:\n",
    "    vocab = make_vocab(source)\n",
    "    print(len(vocab))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Extract context windows"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To build the co-occurrence matrix, we need to define the notion of &lsquo;context&rsquo;. Here we will use **linear contexts**, consisting of the words that precede and follow the target word in a window of $k$ tokens on each side. Your next task is to implement a generator function that extracts all such context windows from the data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"panel panel-primary\">\n",
    "<div class=\"panel-heading\">Problem 2</div>\n",
    "<div class=\"panel-body\">\n",
    "Implement a generator function that yields all context windows for the data. Represent context windows as tuples consisting of $2k+1$ tokens, with the target word in the center component of the tuple.\n",
    "</div>\n",
    "</div>\n",
    "\n",
    "Later in the lab you will use context windows of width $k=2$, but your code should support any width $k \\geq 1$. The windows at the beginning and end of each sentence should be padded with `<bos>` and `<eos>` markers. With this padding, the total number of contexts should equal the number of tokens in the data.\n",
    "\n",
    "To solve the problem, complete the skeleton code in the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "import itertools\n",
    "def contexts(source, k):\n",
    "    # TODO: Replace the following line with your own code\n",
    "    \n",
    "    def window(it, k):\n",
    "        l = list(itertools.islice(it, 2*k+1))\n",
    "     \n",
    "        while 1:\n",
    "            yield tuple(l)\n",
    "            l.append(next(it))\n",
    "            l = l[1:]\n",
    "            \n",
    "    for sentence in source:\n",
    "        \n",
    "        def generate_sentence():\n",
    "            for _ in range(k):\n",
    "                yield b'<bos>'\n",
    "            for token in sentence.split():\n",
    "                yield token\n",
    "            for _ in range(k):\n",
    "                yield b'<eos>'\n",
    "                \n",
    "        yield from window(generate_sentence(), k)\n",
    "        \n",
    " \n",
    "    #next_one = tokens(source)\n",
    "    #next(next_one)\n",
    "    \n",
    "    \n",
    "\n",
    "    \n",
    "    \n",
    "    \n",
    "    #next(tokens(source))\n",
    "    #for j in middle:\n",
    "       # print(j)\n",
    "    #for i, j in zip(middle, nextone):\n",
    "        \n",
    "   #     print(i)\n",
    "    #for prev,cur,next in zip(['<beo>']+tokens(source)[:-1], tokens(source), tokens(source)[1:]+['<eos>']):\n",
    "       # yield (prev, cur, next)\n",
    "    #yield from nlp5.contexts(source, k)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test your code, you can run the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(b'<bos>', b'<bos>', b'april', b'is', b'the')\n",
      "(b'<bos>', b'april', b'is', b'the', b'th')\n",
      "(b'april', b'is', b'the', b'th', b'month')\n",
      "(b'is', b'the', b'th', b'month', b'of')\n",
      "(b'the', b'th', b'month', b'of', b'the')\n",
      "(b'th', b'month', b'of', b'the', b'year')\n",
      "(b'month', b'of', b'the', b'year', b'and')\n",
      "(b'of', b'the', b'year', b'and', b'comes')\n",
      "(b'the', b'year', b'and', b'comes', b'between')\n",
      "(b'year', b'and', b'comes', b'between', b'march')\n"
     ]
    }
   ],
   "source": [
    "from itertools import islice\n",
    "\n",
    "with bz2.open('/home/TDDE09/labs/l5/data/simplewiki.txt.bz2') as source:\n",
    "    for context in islice(contexts(source, 2), 10):    # 10 windows of width k = 2\n",
    "        print(context)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build the co-occurrence matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your next task is to construct the co-occurrence matrix for the data. However, rather than with raw counts, you will fill it with positive pointwise mutual information values.\n",
    "\n",
    "Recall that the **pointwise mutual information (PMI)** between a target word $w$ and a context word $c$ is defined as\n",
    "\n",
    "$$\n",
    "\\text{PMI}(w, c) = \\log \\Biggl( \\frac{\\#(w, c) \\cdot N}{\\#(w) \\cdot \\#(c)} \\Biggr)\n",
    "$$\n",
    "\n",
    "where $\\#(w, c)$ is the number of times $w$ was observed in the same context as $c$, $\\#(w)$ is the total number of times $w$ was observed together with some context word $c$, $\\#(c)$ is the total number of times $c$ was observed together with some target word $w$, and $N$ is the total number of observations. In the case where either the enumerator or the denominator of this expression is zero, we let $\\text{PMI}(w, c) = 0$.\n",
    "\n",
    "**Positive pointwise mutual information (PPMI)** is derived from PMI by clipping all negative values:\n",
    "\n",
    "$$\n",
    "\\text{PPMI}(w, c) = \\max \\bigl(0, \\text{PMI}(w, c) \\bigr)\n",
    "$$\n",
    "\n",
    "Here we will actually use a shifted version of PPMI, where the PMI value is decreased by a constant $\\log \\delta$ before clipping. For $\\delta = 1$, this gives the same result as standard PMI. Higher values of $\\delta$ can improve the performance of word embeddings for different tasks ([Levy and Goldberg, 2014](https://papers.nips.cc/paper/5477-neural-word-embedding-as-implicit-matrix-factorization/)).\n",
    "\n",
    "$$\n",
    "\\text{PPMI}(w, c) = \\max \\bigl(0, \\text{PMI}(w, c) - \\log \\delta \\bigr)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"panel panel-primary\">\n",
    "<div class=\"panel-heading\">Problem 3</div>\n",
    "<div class=\"panel-body\">\n",
    "Write code that builds the shifted PPMI co-occurrence matrix for the data. Represent it as a [SciPy sparse matrix](https://docs.scipy.org/doc/scipy/reference/sparse.html) whose row indices correspond to the target words, and whose column indices correspond to the context words.\n",
    "</div>\n",
    "</div>\n",
    "\n",
    "To solve this problem, complete the skeleton code in the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.sparse import csr_matrix\n",
    "import math\n",
    "def make_ppmi_matrix(vocab, source, k=2, delta=1.0):\n",
    "    \n",
    "    def format_context(context):\n",
    "        only_context = context[0:k] + context[k+1:]\n",
    "        return (list(only_context), context[k])\n",
    "    \n",
    "    word_count = {}\n",
    "    context_count = {}\n",
    "    word_context_count = {}\n",
    "    N = 0\n",
    "    for ctx in contexts(source,k):\n",
    "        only_context, target = format_context(ctx)\n",
    "        \n",
    "        if target in vocab:\n",
    "            \n",
    "            for current_context in only_context:\n",
    "                \n",
    "                if current_context in vocab:\n",
    "                    N += 1\n",
    "                    word_count[target] = word_count.get(target, 0) + 1\n",
    "                    context_count[current_context] = context_count.get(current_context, 0) + 1\n",
    "                    # if target in vocab:\n",
    "                    if target not in word_context_count:\n",
    "                        word_context_count[target] = {}\n",
    "                    word_context_count[target][current_context] = word_context_count[target].get(current_context, 0) + 1\n",
    "       \n",
    "    row = []\n",
    "    col = []\n",
    "    data = []\n",
    "\n",
    "    for target in word_context_count:\n",
    "        for context in word_context_count[target]:\n",
    "            pmi = math.log((word_context_count[target][context]*N)/(word_count[target]*context_count[context]))\n",
    "            ppmi = max(0, pmi - math.log(delta))\n",
    "            if ppmi != 0:\n",
    "                row.append(vocab[target])\n",
    "                col.append(vocab[context])\n",
    "                data.append(ppmi)\n",
    "                \n",
    "    return csr_matrix((data, (row, col)), shape=(max(row)+1, max(col)+1))\n",
    "            \n",
    "    \n",
    "    \n",
    "    \n",
    "    \n",
    "    # TODO: Replace the following line with your own code\n",
    "    #return nlp5.make_ppmi_matrix(vocab, source, k, delta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While the problem of constructing the shifted PPMI matrix is not hard from a conceptual point of view, writing efficient code for it is slightly harder. We recommend to proceed in two steps: First, collect the relevant counts $\\#(w, c)$, $\\#(w)$, $\\#(c)$, and $N$ in standard Python data structures. Then, use these counts to compute the shifted PPMI values and return them as a matrix in [CSR format](https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csr_matrix.html). (See the documentation of scipy.sparse.csr_matrix for an example of how to do this.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can test your code by running it on the small data set:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "with bz2.open('/home/TDDE09/labs/l5/data/simplewiki-small.txt.bz2') as source:\n",
    "    ppmi_matrix = make_ppmi_matrix(small_vocab, source)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should now be able to obtain the PPMI as in the following example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2.9528472185674493"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "ppmi_matrix[small_vocab[b'april'], small_vocab[b'december']]\n",
    "#ppmi_matrix[small_vocab[b'the'], small_vocab[b'month']]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you feel confident that your code is correct, you can run it on the full data. (This will take a while.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with bz2.open('/home/TDDE09/labs/l5/data/simplewiki.txt.bz2') as source:\n",
    "    ppmi_matrix = make_ppmi_matrix(vocab, source)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To avoid re-computing the matrix several times, you can save it to a file as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.sparse\n",
    "\n",
    "# scipy.sparse.save_npz('simplewiki.npz', ppmi_matrix)\n",
    "# ppmi_matrix = scipy.sparse.load_npz('simplewiki.npz')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Storing the PPMI matrix will require approximately 60&nbsp;MB of disk space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build the word embedding"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you have the PPMI matrix, you can construct the word embedding."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"panel panel-primary\">\n",
    "<div class=\"panel-heading\">Problem 4</div>\n",
    "<div class=\"panel-body\">\n",
    "Implement a class representing word embeddings. The class should support the construction of the embedding by means of truncated singular value decomposition.\n",
    "</div>\n",
    "</div>\n",
    "\n",
    "More specifically, we ask you to implement the following interface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from sklearn.decomposition import TruncatedSVD\n",
    "\n",
    "class Embedding(object):\n",
    "    \"\"\"A word embedding.\n",
    "\n",
    "    A word embedding represents words as vectors in a d-dimensional\n",
    "    vector space. The central attribute of the word embedding is a\n",
    "    dense matrix whose rows correspond to the words in some\n",
    "    vocabulary, and whose columns correspond to the d dimensions in\n",
    "    the embedding space.\n",
    "\n",
    "    Attributes:\n",
    "\n",
    "        vocab: The vocabulary, specified as a dictionary that maps\n",
    "            words to integer indices, identifying rows in the embedding\n",
    "            matrix.\n",
    "        dim: The dimensionality of the word vectors.\n",
    "        m: The embedding matrix. The rows of this matrix correspond to the\n",
    "            words in the vocabulary.\n",
    "    \"\"\"\n",
    "    \n",
    "    def __init__(self, vocab, matrix, dim=100):\n",
    "        \"\"\"Initialies a new word embedding.\n",
    "\n",
    "        Args:\n",
    "            vocab: The vocabulary for the embedding, specified as a\n",
    "                dictionary that maps words to integer indices.\n",
    "            matrix: The co-occurrence matrix, represented as a SciPy\n",
    "                sparse matrix with one row and one column for each word in\n",
    "                the vocabulary.\n",
    "            dim: The dimensionality of the word vectors.\n",
    "        \"\"\"\n",
    "        self.vocab = vocab\n",
    "        self.dim = dim\n",
    "        # TODO: Replace the following line with your own code\n",
    "        svd = TruncatedSVD(n_components=dim)\n",
    "        self.m  = svd.fit_transform(matrix)\n",
    "        print(self.m.shape)\n",
    "        # self.m = np.zeros((len(vocab), dim))\n",
    "\n",
    "    def vec(self, w):\n",
    "        \"\"\"Returns the vector for the specified word.\n",
    "\n",
    "        Args:\n",
    "            w: A word, an element of the vocabulary.\n",
    "\n",
    "        Returns:\n",
    "            The word vector for the specified word.\n",
    "        \"\"\"\n",
    "        # TODO: Replace the following line with your own code\n",
    "        return self.m[self.vocab[w]]\n",
    "        #return np.zeros((1, self.dim))\n",
    "\n",
    "    def distance(self, w1, w2):\n",
    "        \"\"\"Computes the cosine similarity between the specified words.\n",
    "        \n",
    "        Args:\n",
    "            w1: The first word (an element of the vocabulary).\n",
    "            w2: The second word (an element of the vocabulary).\n",
    "        \n",
    "        Returns:\n",
    "            The cosine similarity between the specified words in this\n",
    "            embedding.\n",
    "        \"\"\"\n",
    "        w1_vec = self.vec(w1)\n",
    "        w2_vec = self.vec(w2)\n",
    "        \n",
    "        cosine_sim = np.dot(w1_vec, w2_vec) / (np.sqrt(w1_vec.dot(w1_vec))*np.sqrt(w2_vec.dot(w2_vec))) \n",
    "        # TODO: Replace the following line with your own code\n",
    "        return cosine_sim\n",
    "    \n",
    "    def most_similar(self, w, n=10):\n",
    "        \"\"\"Returns the most similar words for the specified word.\n",
    "\n",
    "        Args:\n",
    "            w: A word, an element of the vocabulary.\n",
    "            n: The maximal number of most similar words to return.\n",
    "\n",
    "        Returns:\n",
    "            A list containing distance/word pairs.\n",
    "        \"\"\"\n",
    "        similarities = []\n",
    "        for i in self.vocab:\n",
    "            similarities.append([self.distance(w, i), i])\n",
    "        similarities.sort(key=lambda x: x[0], reverse=True)\n",
    "        \n",
    "        return  similarities[1:11]\n",
    "            \n",
    "            \n",
    "        # TODO: Replace the following line with your own code\n",
    "        #return [(0, b'<none>')] * n\n",
    "\n",
    "    def analogy(self, w1, w2, w3):\n",
    "        \"\"\"Answers an analogy question of the form w1 - w2 + w3 = ?\n",
    "\n",
    "        Args:\n",
    "            w1: A word, an element of the vocabulary.\n",
    "            w2: A word, an element of the vocabulary.\n",
    "            w3: A word, an element of the vocabulary.\n",
    "\n",
    "        Returns:\n",
    "            The word closest to the vector w1 - w2 + w3 that is different\n",
    "            from all the other words.\n",
    "        \"\"\"\n",
    "        w1_vec = self.vec(w1)\n",
    "        w2_vec = self.vec(w2)\n",
    "        w3_vec = self.vec(w3)\n",
    "        \n",
    "        w_vec = w1_vec - w2_vec + w3_vec\n",
    "        \n",
    "        similarities = []\n",
    "        for i in self.vocab:\n",
    "            if (i != w1 and i != w2 and i!=w3) :  \n",
    "                i_vec = self.vec(i)\n",
    "                cosine_sim = np.dot(i_vec, w_vec) / (np.sqrt(i_vec.dot(i_vec))*np.sqrt(w_vec.dot(w_vec))) \n",
    "            \n",
    "            similarities.append([cosine_sim, i])\n",
    "            \n",
    "        similarities.sort(key=lambda x: x[0], reverse=True)\n",
    "        \n",
    "        return similarities[0][1]\n",
    "    \n",
    "        return b'<none>'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall that the **singular value decomposition** of an $m \\times n$ matrix $\\mathbf{M}$ is a factorization of the form $\\mathbf{U}\\mathbf{\\Sigma}\\mathbf{V}^*$ where $\\mathbf{U}$ is an $m \\times m$ matrix of which we may assume that its columns are sorted in decreasing order of importance when it comes to explaining the variance of $\\mathbf{M}$. (Formally, these columns correspond to the singular values in the matrix $\\mathbf{\\Sigma}$.) By truncating $\\mathbf{U}$ after the first $\\mathit{dim}$ columns, we thus obtain an approximation of the original matrix $\\mathbf{M}$. In your case, $\\mathbf{M}$ is the PPMI matrix, and the truncated matrix $\\mathbf{U}$ gives the word vectors of the embedding. To compute the matrix $\\mathbf{U}$, you can use the class [TruncatedSVD](http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.TruncatedSVD.html), which is available in [scikit-learn](http://scikit-learn.org/stable/index.html)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exploring the embedding"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell shows how to initalise a new embedding with the PPMI matrix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1256, 100)\n"
     ]
    }
   ],
   "source": [
    "embedding = Embedding(small_vocab, ppmi_matrix)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some things that you can do with the embedding:\n",
    "\n",
    "#### Word similarity\n",
    "\n",
    "What is the semantic similarity between &lsquo;man&rsquo; and &lsquo;woman&rsquo;?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6475775514245719"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "embedding.distance(b'man', b'woman')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What words are most similar to &lsquo;man&rsquo;?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[0.6360330311295133, b'jesus'],\n",
       " [0.6285192870717092, b'woman'],\n",
       " [0.5563820162688535, b'michelangelo'],\n",
       " [0.5554638260358611, b'child'],\n",
       " [0.5398580231196313, b'who'],\n",
       " [0.5335589515125784, b'person'],\n",
       " [0.5237655244838998, b'father'],\n",
       " [0.5185884195417944, b'story'],\n",
       " [0.51754999656932, b'god'],\n",
       " [0.49302186444653273, b'another']]"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "embedding.most_similar(b'man')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What words are most similar to &lsquo;woman&rsquo;?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[0.6473731125510612, b'child'],\n",
       " [0.6285192870717092, b'man'],\n",
       " [0.5782028237455199, b'her'],\n",
       " [0.5547281523612119, b'mother'],\n",
       " [0.5500353753708882, b'children'],\n",
       " [0.5270467331707862, b'mary'],\n",
       " [0.5253173277832768, b'jesus'],\n",
       " [0.5111322839402144, b'family'],\n",
       " [0.5101005098723612, b'story'],\n",
       " [0.5052516116247496, b'father']]"
      ]
     },
     "execution_count": 26,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "embedding.most_similar(b'woman')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Analogies\n",
    "\n",
    "Here is the famous king &minus; man + woman = ? example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'prince'"
      ]
     },
     "execution_count": 35,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "embedding.analogy(b'king', b'man', b'woman')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When experimenting with other examples, you will find that the embedding picks up common stereotypes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "ename": "KeyError",
     "evalue": "b'doctor'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mKeyError\u001b[0m                                  Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-36-71dd6fbf652b>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0membedding\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0manalogy\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34mb'doctor'\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34mb'man'\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34mb'woman'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-33-1f844e9637b1>\u001b[0m in \u001b[0;36manalogy\u001b[0;34m(self, w1, w2, w3)\u001b[0m\n\u001b[1;32m    104\u001b[0m             \u001b[0;32mfrom\u001b[0m \u001b[0mall\u001b[0m \u001b[0mthe\u001b[0m \u001b[0mother\u001b[0m \u001b[0mwords\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    105\u001b[0m         \"\"\"\n\u001b[0;32m--> 106\u001b[0;31m         \u001b[0mw1_vec\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvec\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mw1\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    107\u001b[0m         \u001b[0mw2_vec\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvec\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mw2\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    108\u001b[0m         \u001b[0mw3_vec\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvec\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mw3\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-33-1f844e9637b1>\u001b[0m in \u001b[0;36mvec\u001b[0;34m(self, w)\u001b[0m\n\u001b[1;32m     50\u001b[0m         \"\"\"\n\u001b[1;32m     51\u001b[0m         \u001b[0;31m# TODO: Replace the following line with your own code\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 52\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mm\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvocab\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mw\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     53\u001b[0m         \u001b[0;31m#return np.zeros((1, self.dim))\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     54\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mKeyError\u001b[0m: b'doctor'"
     ]
    }
   ],
   "source": [
    "embedding.analogy(b'doctor', b'man', b'woman')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model knows the capital of Sweden."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 37,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "b'st'"
      ]
     },
     "execution_count": 37,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "embedding.analogy(b'berlin', b'germany', b'sweden')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The embedding also &lsquo;learns&rsquo; some syntactic analogies, such as the analogy between the past-tense and present-tense forms of verbs (here: *jump* and *eat*):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "ename": "KeyError",
     "evalue": "b'jumped'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mKeyError\u001b[0m                                  Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-38-ddc130e4cb70>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0membedding\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0manalogy\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34mb'jumped'\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34mb'jump'\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34mb'eat'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-33-1f844e9637b1>\u001b[0m in \u001b[0;36manalogy\u001b[0;34m(self, w1, w2, w3)\u001b[0m\n\u001b[1;32m    104\u001b[0m             \u001b[0;32mfrom\u001b[0m \u001b[0mall\u001b[0m \u001b[0mthe\u001b[0m \u001b[0mother\u001b[0m \u001b[0mwords\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    105\u001b[0m         \"\"\"\n\u001b[0;32m--> 106\u001b[0;31m         \u001b[0mw1_vec\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvec\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mw1\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m    107\u001b[0m         \u001b[0mw2_vec\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvec\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mw2\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m    108\u001b[0m         \u001b[0mw3_vec\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvec\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mw3\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-33-1f844e9637b1>\u001b[0m in \u001b[0;36mvec\u001b[0;34m(self, w)\u001b[0m\n\u001b[1;32m     50\u001b[0m         \"\"\"\n\u001b[1;32m     51\u001b[0m         \u001b[0;31m# TODO: Replace the following line with your own code\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 52\u001b[0;31m         \u001b[0;32mreturn\u001b[0m \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mm\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mvocab\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0mw\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     53\u001b[0m         \u001b[0;31m#return np.zeros((1, self.dim))\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     54\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mKeyError\u001b[0m: b'jumped'"
     ]
    }
   ],
   "source": [
    "embedding.analogy(b'jumped', b'jump', b'eat')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

import bz2
import itertools
from itertools import islice
from scipy.sparse import csr_matrix
import scipy.sparse
import math
import numpy as np
from sklearn.decomposition import TruncatedSVD

def tokens(source):
    for sentence in source:
        for token in sentence.split():
            yield token

def make_vocab(source, threshold=100):
    
    vocabulary = {}
    for next_word in tokens(source):
        vocabulary[next_word] = vocabulary.get(next_word, 0) + 1
    v_with_threshold = {k: v for k, v in vocabulary.items() if v >= threshold}
    i = 0
    for word in v_with_threshold:
        v_with_threshold[word] = i
        i += 1
        
    return v_with_threshold


def contexts(source, k):
    # TODO: Replace the following line with your own code
    
    def window(it, k):
        l = list(itertools.islice(it, 2*k+1))
     
        while 1:
            yield tuple(l)
            l.append(next(it))
            l = l[1:]
            
    for sentence in source:
        
        def generate_sentence():
            for _ in range(k):
                yield b'<bos>'
            for token in sentence.split():
                yield token
            for _ in range(k):
                yield b'<eos>'
                
        yield from window(generate_sentence(), k)


def make_ppmi_matrix(vocab, source, k=2, delta=1.0):
    
    def format_context(context):
        only_context = context[0:k] + context[k+1:]
        return (list(only_context), context[k])
    
    word_count = {}
    context_count = {}
    word_context_count = {}
    N = 0
    for ctx in contexts(source,k):
        only_context, target = format_context(ctx)
        
        if target in vocab:
            
            for current_context in only_context:
                
                if current_context in vocab:
                    N += 1
                    word_count[target] = word_count.get(target, 0) + 1
                    context_count[current_context] = context_count.get(current_context, 0) + 1
                    # if target in vocab:
                    if target not in word_context_count:
                        word_context_count[target] = {}
                    word_context_count[target][current_context] = word_context_count[target].get(current_context, 0) + 1
       
    row = []
    col = []
    data = []

    for target in word_context_count:
        for context in word_context_count[target]:
            pmi = math.log((word_context_count[target][context]*N)/(word_count[target]*context_count[context]))
            ppmi = max(0, pmi - math.log(delta))
            if ppmi != 0:
                row.append(vocab[target])
                col.append(vocab[context])
                data.append(ppmi)
                
    return csr_matrix((data, (row, col)), shape=(max(row)+1, max(col)+1))


class Embedding(object):
    """A word embedding.

    A word embedding represents words as vectors in a d-dimensional
    vector space. The central attribute of the word embedding is a
    dense matrix whose rows correspond to the words in some
    vocabulary, and whose columns correspond to the d dimensions in
    the embedding space.

    Attributes:

        vocab: The vocabulary, specified as a dictionary that maps
            words to integer indices, identifying rows in the embedding
            matrix.
        dim: The dimensionality of the word vectors.
        m: The embedding matrix. The rows of this matrix correspond to the
            words in the vocabulary.
    """
    
    def __init__(self, vocab, matrix, dim=100):
        """Initialies a new word embedding.

        Args:
            vocab: The vocabulary for the embedding, specified as a
                dictionary that maps words to integer indices.
            matrix: The co-occurrence matrix, represented as a SciPy
                sparse matrix with one row and one column for each word in
                the vocabulary.
            dim: The dimensionality of the word vectors.
        """
        self.vocab = vocab
        self.dim = dim
        # TODO: Replace the following line with your own code
        svd = TruncatedSVD(n_components=dim)
        self.m  = svd.fit_transform(matrix)
        print(self.m.shape)
        # self.m = np.zeros((len(vocab), dim))

    def vec(self, w):
        """Returns the vector for the specified word.

        Args:
            w: A word, an element of the vocabulary.

        Returns:
            The word vector for the specified word.
        """
        # TODO: Replace the following line with your own code
        return self.m[self.vocab[w]]
        #return np.zeros((1, self.dim))

    def distance(self, w1, w2):
        """Computes the cosine similarity between the specified words.
        
        Args:
            w1: The first word (an element of the vocabulary).
            w2: The second word (an element of the vocabulary).
        
        Returns:
            The cosine similarity between the specified words in this
            embedding.
        """
        w1_vec = self.vec(w1)
        w2_vec = self.vec(w2)
        
        cosine_sim = np.dot(w1_vec, w2_vec) / (np.sqrt(w1_vec.dot(w1_vec))*np.sqrt(w2_vec.dot(w2_vec))) 
        # TODO: Replace the following line with your own code
        return cosine_sim
    
    def most_similar(self, w, n=10):
        """Returns the most similar words for the specified word.

        Args:
            w: A word, an element of the vocabulary.
            n: The maximal number of most similar words to return.

        Returns:
            A list containing distance/word pairs.
        """
        similarities = []
        for i in self.vocab:
            similarities.append([self.distance(w, i), i])
        similarities.sort(key=lambda x: x[0], reverse=True)
        
        return  similarities[1:11]
            
            
        # TODO: Replace the following line with your own code
        #return [(0, b'<none>')] * n

    def analogy(self, w1, w2, w3):
        """Answers an analogy question of the form w1 - w2 + w3 = ?

        Args:
            w1: A word, an element of the vocabulary.
            w2: A word, an element of the vocabulary.
            w3: A word, an element of the vocabulary.

        Returns:
            The word closest to the vector w1 - w2 + w3 that is different
            from all the other words.
        """
        w1_vec = self.vec(w1)
        w2_vec = self.vec(w2)
        w3_vec = self.vec(w3)
        
        w_vec = w1_vec - w2_vec + w3_vec
        
        similarities = []
        for i in self.vocab:
            if (i != w1 and i != w2 and i!=w3) :  
                i_vec = self.vec(i)
                cosine_sim = np.dot(i_vec, w_vec) / (np.sqrt(i_vec.dot(i_vec))*np.sqrt(w_vec.dot(w_vec))) 
            
            similarities.append([cosine_sim, i])
            
        similarities.sort(key=lambda x: x[0], reverse=True)
        
        return similarities[0][1]
#print("Creating small_vocab...")
#with bz2.open('simplewiki-small.txt.bz2') as source:
#    small_vocab = make_vocab(source)
#    print("Done.")

print("Creating vocab...")
with bz2.open('simplewiki.txt.bz2') as source:
    vocab = make_vocab(source)
print("Done.")

#print("Creating PPMI matrix for small vocab...")
#with bz2.open('simplewiki-small.txt.bz2') as source:
#    ppmi_matrix_small = make_ppmi_matrix(small_vocab, source)
#print("Done.")

#print("Creating PPMI matrix for vocab...")
#with bz2.open('simplewiki.txt.bz2') as source:
#    ppmi_matrix = make_ppmi_matrix(vocab, source)
#print("Done")
print("Opening PPMI...")
#scipy.sparse.save_npz('simplewiki.npz', ppmi_matrix)
ppmi_matrix = scipy.sparse.load_npz('simplewiki.npz')
print("Done.")

embedding = Embedding(vocab, ppmi_matrix)
print(embedding.distance(b'man', b'woman'))
print(embedding.most_similar(b'man'))
print(embedding.analogy(b'king', b'man', b'woman'))
print(embedding.analogy(b'doctor', b'man', b'woman'))
print(embedding.analogy(b'berlin', b'germany', b'sweden'))


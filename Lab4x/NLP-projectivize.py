# Run the program the following way :
# python NLP-projectivize.py < en-ud-dev.conllu > en-ud-dev-proj.conllu
# (need to use python 3)

def trees(fp):
    buffer = []
    for line in fp:
        line = line.rstrip() # strip off the trailing newline
        if not line.startswith('#'):
            if len(line) == 0:
                yield buffer
                buffer = []
            else:
                columns = line.split()
                if columns[0].isdigit(): # skip range tokens
                    buffer.append(columns)

def cmd_projectivize():
    import sys
    for ptree in projectivized_trees(sys.stdin):
        emit(ptree)

def projectivized_trees(fp):
    for tree in trees(fp):

        pheads = projectivize(heads(tree))
        for i, row in enumerate(tree):
            row[6] = "%d" % pheads[i+1]
        yield tree

def emit(tree):
    for row in tree:
        print("\t".join(row))
    print()

def heads(rows):
    return [0] + [int(row[6]) for row in rows]

def projectivize(tree):
    n = len(tree)
    # Compute the costs for the different arcs
    A =  [[float("inf")]*n for _ in range(n)]
    for a in range(0, n):
        for d in range(0, n):
            cost = 0
            d_up = d
            while tree[d_up] != a and tree[d_up] != 0:
                d_up = tree[d_up]
                cost+=1
            if tree[d_up] == a:
                A[a][d] = cost

    T1 = [[0]*n for _ in range(n)]
    T2 = [[0]*n for _ in range(n)]
    T3 = [[0]*n for _ in range(n)]
    T4 = [[0]*n for _ in range(n)]


    T1_back_ptr = [[{}]*n for _ in range(n)]
    T2_back_ptr = [[{}]*n for _ in range(n)]
    T3_back_ptr = [[{}]*n for _ in range(n)]
    T4_back_ptr = [[{}]*n for _ in range(n)]
    for k in range(1,n):                                
        for i in range(k-1, -1, -1):                    
            cur_value = float("inf")
            best_j = 0
            for j in range(i,k):
                new_value = T2[i][j] + T1[j+1][k] + A[i][k]
                if cur_value > new_value:
                    cur_value = new_value
                    best_j = j
            T4[i][k] = cur_value
            T4_back_ptr[i][k] = {"T2": (i, best_j), "T1" :(best_j+1, k), "A":(i, k)}

            cur_value = float("inf")
            best_j = 0
            for j in range(i,k):
                new_value = T2[i][j] + T1[j+1][k] + A[k][i]
                if cur_value > new_value:
                    cur_value = new_value
                    best_j = j
            T3[i][k] = cur_value
            T3_back_ptr[i][k] = {"T2": (i, best_j), "T1" :(best_j+1, k), "A":(k, i)}

            cur_value = float("inf")
            best_j = 0
            for j in range(i+1,k+1):                   
                new_value = T4[i][j] + T2[j][k]
                if cur_value > new_value:
                    cur_value = new_value
                    best_j = j
            T2[i][k] = cur_value
            T2_back_ptr[i][k] = {"T4": (i, best_j ), "T2":(best_j , k)}

            cur_value = float("inf")
            best_j = 0
            for j in range(i,k):
                new_value = T1[i][j] + T3[j][k]
                if cur_value > new_value:
                    cur_value = new_value
                    best_j = j
            T1[i][k] = cur_value
            T1_back_ptr[i][k] = {"T1": (i, best_j), "T3": (best_j, k)}

    build_tree = [0]*n


    map_string_to_array = {"T1":T1_back_ptr, "T2":T2_back_ptr, "T3":T3_back_ptr, "T4":T4_back_ptr}

    def recreate_tree(curr_table, start, end):
        if start != end:
            for sub_tree, values in curr_table[start][end].items():
                if sub_tree == "A":
                    #print(values)
                    build_tree[values[1]] = values[0]
                else:
                    recreate_tree(map_string_to_array[sub_tree], values[0], values[1])

    recreate_tree(T2_back_ptr, 0, n-1)
    return build_tree

if __name__ == "__main__":
    cmd_projectivize()

---
title: Lab2X - Report
author: Loïs Bilat, Goutam Bhat
date: \today
---

# Lab2x

## Problem 1

There are 484 possible characters for prononcation *yi*.

## Problem 2

We implemented a language model using the Witten-Bell smoothing. using recursivity. When training our modle, we used the ' ' character to denote the beggining of a new line. We chose do this because we wanted to group chinese words starting at a new line and chinese words starting after a username. With a specific character for the beggining of line we obtain slighly worse accuracy.

We implemented a language model using the Witten-Bell smoothing using recursivity. When training
our model, we used the “ ” character (space) as a beginning of sentence (BoS) character. Since we modelled  the language on chinese characters, not words, we felt that using space as a BoS character was more appropriate. It also allows us to group chinese words starting at a new line and chinese words starting after a username together. In both case, this is a new chinese sentence so they should be treated the same way. With a special character (e.g. `'\r'`) for the BoS, we obtained slightly worse accuracy.

## Problem 3
 
The predictions for the first 10 positions are printed when running the program.

## Problem 4

We get an accuracy of 80.2069% when using a trigram model. With smaller or greater models, we get a worse accuracy 




import sys

vocabulary = set()

class Model(object):

    def __init__(self, order):
        self.order = order
        self.count_context = {}
        self.count_context_char = {}
        self.bos = ' '
        if order > 0: # If the order is 1, we don't have a smaller recursive order
            self.smaller_model = Model(order-1)

    def get_order(self):
        return self.order

    def train(self,training_data):
        order = self.order
        count_context = self.count_context
        count_context_char = self.count_context_char

        for line in training_data:
            for w_end in range (1, len(line)):
                w_begin = w_end - order
                word = ""
                if w_begin < 0: # If we are at the begginign of the line, add the correct character
                    word = line[0:w_end]

                    word = self.bos + word
                else:
                    word = line[w_begin:w_end]

                # Update statistics for the context and the context_char
                if word in count_context_char:
                    count_context_char[word] += 1
                else:
                    count_context_char[word] = 1

                context = word[0:-1]
                if context in count_context:
                    count_context[context] += 1
                else:
                    count_context[context] = 1


        if (order > 0): # Train another model with order n-1
            self.smaller_model.train(training_data)

    def predict(self,context, char):

        # If the given is too small, add a beggining of line character
        if len(context) < self.order - 1 :
            context = self.bos + context


        context_char = context + char


        # Compute the left_side of the witten-bell formula
        first_side = 0
        new_lambda = 0
        if context_char in self.count_context_char:
            if context in self.count_context:
                new_lambda = self.lamb(context)
                first_side = new_lambda*self.count_context_char[context_char]/self.count_context[context]

        # Compute the right-side of the formula, with the recursive call if needed
        second_side = 0
        if (self.order > 1):
            second_side = (1 - new_lambda)*self.smaller_model.predict(context[1:], char)
        else:
            second_side = (1 - new_lambda) / len(self.count_context_char)

        return first_side + second_side

    def lamb(self,context):
        return self.count_context[context] / (self.count_context[context] + len(self.count_context))




# Return all the lines of a file
def open_data (file_name):
    file = open(file_name,'r')
    return file.readlines()

# Put the char map in a usable data-structure
def read_char_map (file_name):
    file = open(file_name,'r')
    lines = file.readlines()

    charmap = {}

    for l in lines:
        char = l[0]
        pron = l[2:-1]
        if pron in charmap:
            charmap[pron].append(char)
        else:
            charmap[pron] = [char]
    return charmap




def test_prediction(test_data, charmap, model):
    out_file = []

    first_line = True # We have to print the prediciton probabilites for the first positions
    for line in test_data:
        words = line.split(" ")
        output_line = []
        output_string = ""

        for word in words:
            eol = False
            if (word[len(word)-1] == '\n'): # remove the \n character from the word read
                word = word[0:-1]
                eol = True

            if word == "<space>": # handle <space
                output_line.append(" ")
                output_string += " "
                best_char = " "
                best_prob = 1

            elif word in charmap:

                # get all posible characters for the prononciation
                all_pronunciations = charmap[word]

                # Get the context
                if len(output_string) >= model.get_order() - 1 :
                    context = output_string[-model.get_order()+1:]
                else:
                    # Beg of sentence
                    context = model.bos + output_string

                best_char = all_pronunciations[0]
                best_prob = -1

                for char in all_pronunciations:
                    # Compute prob of all characters
                    new_prob = model.predict(context, char)
                    if new_prob > best_prob:
                        best_prob = new_prob
                        best_char = char
                # If single character, might be considered as an english word's character        
                if len(word) == 1:
                    english_pred = model.predict(context, word)
                    if english_pred > best_prob:
                        best_char = word
                        best_prob = english_pred

                output_line.append(best_char)
                output_string += best_char

            else: # If no chinese character asociated, just output the same 
                best_char = word
                best_prob = 1
                output_string += word
                output_line.append(word)

            if eol: # Put the endline we removed before
                output_string += '\n'

            if first_line: # To print probs for the first positions, as required
                print(word + " ---- " + best_char + " ---- " + str(best_prob))

        out_file.append(output_line)

        first_line = False

    return(out_file)

charmap = read_char_map("charmap")


# get the accuracy between the prediction and the true data
# Print in color the prediction's correctness depending on input parameter print_output
def get_accuracy(pred_data, true_data, print_output) :

    total_ct = 0
    correct_ct = 0

    for n_pred, n_true in zip(pred_data, true_data) :
        for nn_pred, nn_true in zip(n_pred, n_true) :
            total_ct += 1
            if nn_pred == nn_true :
                if print_output:
                    print('\033[92m' + nn_pred, end='')
                correct_ct += 1
            else :
                if print_output :
                    print('\033[91m' +nn_pred, end='')

        if print_output:
            print()

    if print_output:
        print('\033[97m')
    accuracy = correct_ct*100 / total_ct
    return accuracy

# To print the text stored in a 2D array as a string
def print_pred_data(pred_data) :
    for line in pred_data:
        for word in line:
            print(word, end='')

        print('\n')

print("Number of characters with pronounciation yi: ", end='')
print(len(charmap["yi"]))

print("Training a trigram model...\n")
train_data = open_data("train.han")
m = Model(3)
m.train(train_data)

test_data = open_data("test.pin")
pred_output = test_prediction(test_data, charmap, m)

true_data = open_data("test.han")

print("\nWe get the following accuracy : ")
print(str(get_accuracy(pred_output, true_data, len(sys.argv) > 1)) + "%")

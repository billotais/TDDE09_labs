# L1X: Maximum Entropy Classification

import json
import numpy as np

def load_data(filename):
    """Loads movie review data."""
    result = []
    with open(filename) as source:
        for review in json.load(source):
            text = review['text'].split()
            polarity = review['polarity']
            result.append((text, polarity))
    return result

# Problem 1

# In order to represent a movie review as a NumPy vector, we need to
# assign to each word in our training set a unique integer index which
# will give us that component in the vector which is 1 if the
# corresponding word is present in the review, and 0 otherwise.

def build_w2i(data):
    """Returns a dictionary that maps words to integers."""
    
    word_to_index = {}
    next_index = 0
    
    for pair in data:
        feat = pair[0]
        
        for word in feat:
            if word not in word_to_index :
                word_to_index[word] = next_index
                next_index +=1
            
            
    return word_to_index

def featurize(data, w2i):
    """Converts review data into matrix format

    The argument w2i is a word index, as described above. The function
    should return a pair of NumPy matrices X, Y, where X is an N-by-F
    matrix (N: number of instances in the data, F: number of features,
    here: number of unique words in the data), and where Y is an
    N-by-K matrix (K: number of classes, here: 2).

    """
    # Initialize
    X = np.zeros(shape=(len(data), len(w2i)))
    Y = np.zeros(shape=(len(data), 2 ) )
    
    # Loop over data
    for data_ind in range(0, len(data)):
        feat = data[data_ind]
        
        for word in feat[0]:
            if word in w2i:
                X[data_ind, w2i[word]] = 1
                
        if feat[1] == "pos":
            Y[data_ind, 0] = 1
            Y[data_ind, 1] = 0
        elif  feat[1] == "neg":
            Y[data_ind, 0] = 0
            Y[data_ind, 1] = 1
        else:
            print("Error, weird label")
            
        
    return X, Y

# Problem 2

def minibatches(X, Y, batch_size):
    """Yields mini-batches from the specified X and Y matrices."""
    m = X.shape[0]
    n_batches = int(np.floor(m / batch_size))
    random_indices = np.random.permutation(np.arange(m))
    for i in range(n_batches):
        batch_indices = np.arange(i * batch_size, (i + 1) * batch_size)
        batch_indices = random_indices[batch_indices]
        yield X[batch_indices], Y[batch_indices]

def softmax(X):
    """Computes the softmax function for the specified batch of data.

    The implementation uses a common trick to improve numerical
    stability; this trick is explained here:

    http://stackoverflow.com/a/39558290

    """
    E = np.exp(X - np.max(X, axis=1, keepdims=True))
    return E / E.sum(axis=1, keepdims=True)

class MaxentClassifier(object):

    def __init__(self, n_features, n_classes):
        self.W = np.zeros((n_features, n_classes))

    def p(self, X):
        """Returns the class probabilities for the given input."""
        
        raw_scores = np.dot(X, self.W)
        
        prob = softmax(raw_scores)
        
        return prob

    def predict(self, X):
        """Returns the most probable class for the given input."""
        prob = self.p(X)
            
        max_ind = np.argmax(prob, axis=1) 
        
        return max_ind

    @classmethod
    def train(cls, X, Y, n_epochs=1, batch_size=1, eta=0.01, rho=0.1):
        """Trains a new classifier."""
        classifier = cls(X.shape[1], Y.shape[1])
        
        for ep in range(0, n_epochs):
            for x_batch, y_batch in (minibatches(X, Y, batch_size)):
                
                classifier.W = classifier.W + eta*( np.dot( np.transpose(x_batch), (y_batch - softmax( np.dot(x_batch, classifier.W) )) ) - rho*classifier.W)
                
        return classifier

# Problem 3

if __name__ == "__main__":
    import sys

    # Seed the random number generator to get reproducible results
    np.random.seed(42)

    # Load the training data and featurize it
    training_data = load_data(sys.argv[1])
    w2i = build_w2i(training_data)
    X, Y = featurize(training_data, w2i)

    # Train the classifier
    classifier = MaxentClassifier.train(X, Y, 5, 18, 0.01, 0.1)

    # Compute the accuracy of the trained classifier on the test data
    test_data = load_data(sys.argv[2])
    X, Y = featurize(test_data, w2i)
    print(np.mean(classifier.predict(X) == np.argmax(Y, axis=1)))

    # The obtained accuracy is 85.5%


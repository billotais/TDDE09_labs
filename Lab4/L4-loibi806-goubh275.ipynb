{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-danger\">\n",
    "**Due date:** 2018-02-16\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# L4: Syntactic analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Syntactic analysis, also called syntactic parsing, is the task of mapping a sentence to a formal representation of its syntactic structure. In this lab you will implement a syntactic parser that produces **dependency trees**. A dependency tree consists of directed arcs between individual words (tokens) of a sentence. To see some examples, have a look at the [Syntax: General Principles](http://universaldependencies.org/u/overview/syntax.html) page of the [Universal Dependencies Project](http://universaldependencies.org/).\n",
    "\n",
    "Your starting point for this lab is a complete reference implementation of a syntactic parser in the Python module `nlp4`. This system (which consists of roughly 200 lines of code) implements a simple pipeline architecture that includes a variant of the part-of-speech tagger that you implemented in lab&nbsp;3 and a transition-based dependency parser based on the arc-standard algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nlp4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reference system has the following components:\n",
    "\n",
    "1. code to read in dependency trees in the [CoNLL-U Format](http://universaldependencies.org/format.html)\n",
    "2. a class that implements a multi-class perceptron classifier (lab&nbsp;1)\n",
    "3. a class that implements a part-of-speech tagger (lab&nbsp;3)\n",
    "4. a class that implements a transition-based dependency parser (this lab)\n",
    "5. code to train and evaluate the parser on a treebank in the CoNLL-U format\n",
    "\n",
    "These components are explained in more detail below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-warning\">\n",
    "For this lab you only need to implement the functions that read dependency trees (Problem&nbsp;1) and the class that implements the parser (Problem&nbsp;2). As for the other components, you will import them from the reference system.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data set that you will be using in this lab is the [English Web Treebank](http://universaldependencies.org/en/overview/introduction.html) from the [Universal Dependencies Project](http://universaldependencies.org/). This is a corpus of written English taken from five genres of web media: weblogs, newsgroups, emails, reviews, and Yahoo! answers. Each sentence in the corpus was semi-automatically annotated with (among other things) part-of-speech tags and syntactic dependency trees.\n",
    "\n",
    "In the original files from the Universal Dependencies Project, annotated sentences are stored in the [CoNLL-U Format](http://universaldependencies.org/format.html). This is a simple text-based format where a sentence is represented with one line per word (token), and where each line contains 10 tab-separated fields with various pieces of information about the word in question &ndash; including the word form and the part-of-speech tag.\n",
    "\n",
    "The following code cell prints a concrete example, which shows how the data looks like when it comes to Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[['1', 'From', 'from', 'ADP', 'IN', '_', '3', 'case', '_', '_'], ['2', 'the', 'the', 'DET', 'DT', 'Definite=Def|PronType=Art', '3', 'det', '_', '_'], ['3', 'AP', 'AP', 'PROPN', 'NNP', 'Number=Sing', '4', 'nmod', '_', '_'], ['4', 'comes', 'come', 'VERB', 'VBZ', 'Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin', '0', 'root', '_', '_'], ['5', 'this', 'this', 'DET', 'DT', 'Number=Sing|PronType=Dem', '6', 'det', '_', '_'], ['6', 'story', 'story', 'NOUN', 'NN', 'Number=Sing', '4', 'nsubj', '_', '_'], ['7', ':', ':', 'PUNCT', ':', '_', '4', 'punct', '_', '_']]\n"
     ]
    }
   ],
   "source": [
    "with open(\"/home/TDDE09/labs/l4/data/en-ud-dev.conllu\") as fp:\n",
    "    print(next(nlp4.conllu(fp)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, each sentence is represented as a list of lists of strings. The inner lists correspond to the word lines, and the elements of the inner lists correspond to the tab-separated fields. Take a moment to think about how these elements map to the fields as they are described on the [CoNLL-U Format](http://universaldependencies.org/format.html) page. In particular, try to identify the list indices that correspond to the fields holding the word form, part-of-speech tag, and syntactic head of the relevant word."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"panel panel-primary\">\n",
    "<div class=\"panel-heading\">Problem 1</div>\n",
    "<div class=\"panel-body\">\n",
    "Implement a generator function `trees()` that reads dependency trees stored in the CoNLL-U format, and for each tree yields a triple consisting of the list of words in the sentence (strings), the corresponding list of part-of-speech tags (strings), and the syntactic heads for all of the words (integers).\n",
    "</div>\n",
    "</div>\n",
    "\n",
    "This is how the output of the function `trees()` should look like for the sample sentence:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(['<ROOT>', 'From', 'the', 'AP', 'comes', 'this', 'story', ':'], ['<ROOT>', 'ADP', 'DET', 'PROPN', 'VERB', 'DET', 'NOUN', 'PUNCT'], [0, 3, 3, 4, 0, 6, 4, 4])\n"
     ]
    }
   ],
   "source": [
    "with open(\"/home/TDDE09/labs/l4/data/en-ud-dev.conllu\") as fp:\n",
    "    print(next(nlp4.trees(fp)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that you are supposed to explicitly convert the head fields from strings to integers. For technical reasons, you are also supposed to pre-pad each sentence with a special pseudo-word `<ROOT>`. The part-of-speech tag of this pseudo-word should be `<ROOT>`, and its head should have index&nbsp;0.\n",
    "\n",
    "To solve Problem&nbsp;1, you should modify the following code cell. Please note that you are supposed to write this function as a [generator function](https://wiki.python.org/moin/Generators). In particular, the function should *not* return a list of triples. You may use the function `nlp4.conllu()` for reading the raw data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [],
   "source": [
    "def trees(source):\n",
    "    \"\"\"Reads trees from an input source.\n",
    "    \n",
    "    Args:\n",
    "        source: An iterable, such as a file pointer.\n",
    "    \n",
    "    Yields:\n",
    "        Triples of the form `words`, `tags`, heads where: `words` is\n",
    "        the list of words of the tree (including the pseudo-word\n",
    "        <ROOT> at position 0), `tags` is the list of corresponding\n",
    "        part-of-speech tags, and `heads` is the list of head indices\n",
    "        (one head index per word in the tree).\n",
    "    \"\"\"\n",
    "    # TODO: Replace the following line with your own code\n",
    "    data = nlp4.conllu(source)\n",
    "    for row in data:\n",
    "        words = ['<ROOT>']\n",
    "        pos_tag = ['<ROOT>']\n",
    "        arcs = [0]\n",
    "        for word in row:\n",
    "            words.append(word[1])\n",
    "            pos_tag.append(word[3])\n",
    "            arcs.append(int(word[6]))\n",
    "    \n",
    "        yield (words, pos_tag, arcs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Test your code by executing the following code cell. When you are done with Problem&nbsp;1, this should produce exactly the same output as the call to `nlp4.trees()` above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(['<ROOT>', 'From', 'the', 'AP', 'comes', 'this', 'story', ':'], ['<ROOT>', 'ADP', 'DET', 'PROPN', 'VERB', 'DET', 'NOUN', 'PUNCT'], [0, 3, 3, 4, 0, 6, 4, 4])\n"
     ]
    }
   ],
   "source": [
    "with open(\"/home/TDDE09/labs/l4/data/en-ud-dev.conllu\") as fp:\n",
    "    print(next(trees(fp)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The multi-class perceptron classifier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first core component of the baseline system is a multi-class perceptron classifier, very much like the one that you have implemented in lab&nbsp;1 (on text classification), and then again in lab&nbsp;3 (on part-of-speech tagging).\n",
    "\n",
    "### Implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Perceptron(nlp4.Perceptron):\n",
    "    \"\"\"A multi-class perceptron classifier.\n",
    "    \n",
    "    A multi-class perceptron consists of a number of cells, one cell\n",
    "    for each class. When a cell is presented with an input, it gets\n",
    "    activated, and the cell with the highest activation predicts the\n",
    "    class for the input. An input is represented by a feature vector,\n",
    "    and the activation is computed by taking the dot product (weighted\n",
    "    sum) of this feature vector and the cell-specific weight vector.\n",
    "    \n",
    "    This implementation of a multi-class perceptron assumes that both\n",
    "    classes and features can be used as dictionary keys. Feature\n",
    "    vectors are represented as lists of features.\n",
    "    \n",
    "    Attributes:\n",
    "        classes: A set containing the classes of this classifier.\n",
    "        weights: A nested dictionary mapping feature–class\n",
    "            combinations to class-specific feature weights (floats).\n",
    "    \"\"\"\n",
    "\n",
    "    def __init__(self):\n",
    "        \"\"\"Initialises a new classifier.\"\"\"\n",
    "        super().__init__()\n",
    "\n",
    "    def predict(self, x, candidates=None):\n",
    "        \"\"\"Predicts the class for a feature vector.\n",
    "        \n",
    "        This computes the activations for the classes of this\n",
    "        perceptron for the feature vector `x` and returns the class\n",
    "        with the highest activation.\n",
    "        \n",
    "        Args:\n",
    "            x: A feature vector (a list of features).\n",
    "            candidates: A list of candidate classes, or `None` if all\n",
    "                classes defined in this perceptron should be\n",
    "                considered as candidates.\n",
    "        \n",
    "        Returns:\n",
    "            The candidate class with the highest activation. If\n",
    "            several classes have the same activation, returns the\n",
    "            class that is largest with respect to the standard\n",
    "            ordering on classes.\n",
    "        \"\"\"\n",
    "        return super().predict(x, candidates)\n",
    "\n",
    "    def update(self, x, y):\n",
    "        \"\"\"Updates the weight vectors with a single training instance.\n",
    "        \n",
    "        Args:\n",
    "            x: A feature vector.\n",
    "            y: The gold-standard class for the specified feature\n",
    "                vector.\n",
    "        \n",
    "        Returns:\n",
    "            The predicted class for the specified feature vector.\n",
    "        \"\"\"\n",
    "        return super().update(x, y)\n",
    "\n",
    "    def finalize(self):\n",
    "        \"\"\"Averages the weight vectors.\"\"\"\n",
    "        super().finalize()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The only novelty of this implementation compared to the ones that you have produced earlier is in the `predict()` method. This method now takes an optional argument `candidates`, which may be either `None` (the default value) or a list of classes. When it is the latter, the perceptron restricts its prediction to the specified classes, that is, it predicts the highest-scoring class from the list, rather than the highest-scoring class overall. This functionality will be useful in the parser."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The part-of-speech tagger"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The second core component of the baseline system is a part-of-speech tagger, very similar to the one that you have implemented in lab&nbsp;3.\n",
    "\n",
    "### Implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Tagger(nlp4.Tagger):\n",
    "    \"\"\"A part-of-speech tagger based on a multi-class perceptron\n",
    "    classifier.\n",
    "    \n",
    "    This tagger implements a simple, left-to-right tagging algorithm\n",
    "    where the prediction of the tag for the next word in the sentence\n",
    "    can be based on the surrounding words and the previously\n",
    "    predicted tags. The exact features that this prediction is based\n",
    "    on can be controlled with the `features()` method, which should\n",
    "    return a feature vector that can be used as an input to the\n",
    "    multi-class perceptron.\n",
    "    \n",
    "    Attributes:\n",
    "        classifier: A multi-class perceptron classifier.\n",
    "    \"\"\"\n",
    "    \n",
    "    def __init__(self):\n",
    "        \"\"\"Initialises a new tagger.\"\"\"\n",
    "        super().__init__()\n",
    "    \n",
    "    def tag(self, words):\n",
    "        \"\"\"Tags a sentence with part-of-speech tags.\n",
    "        \n",
    "        Args:\n",
    "            words: The input sentence, a list of words.\n",
    "        \n",
    "        Returns:\n",
    "            The list of predicted tags for the input sentence.\n",
    "        \"\"\"\n",
    "        return super().tag(words)\n",
    "    \n",
    "    def update(self, words, gold_tags):\n",
    "        \"\"\"Updates the tagger with a single training example.\n",
    "        \n",
    "        Args:\n",
    "            words: The list of words in the input sentence.\n",
    "            gold_tags: The list of gold-standard tags for the input\n",
    "                sentence.\n",
    "        \n",
    "        Returns:\n",
    "            The list of predicted tags for the input sentence.\n",
    "        \"\"\"\n",
    "        return super().update(words, gold_tags)\n",
    "    \n",
    "    def features(self, words, i, pred_tags):\n",
    "        \"\"\"Extracts features for the specified tagger configuration.\n",
    "        \n",
    "        Args:\n",
    "            words: The input sentence, a list of words.\n",
    "            i: The index of the word that is currently being tagged.\n",
    "            pred_tags: The list of previously predicted tags.\n",
    "        \n",
    "        Returns:\n",
    "            A feature vector for the specified configuration.\n",
    "        \"\"\"\n",
    "        return super().features(words, i, pred_tags)\n",
    "    \n",
    "    def finalize(self):\n",
    "        \"\"\"Averages the weight vectors.\"\"\"\n",
    "        super().finalize()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reference implementation extracts the following features: current word, previous word, next word, and previous tag."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The parser"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your main task in this lab is to implement the third and final core component of the baseline system, the parser."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"panel panel-primary\">\n",
    "<div class=\"panel-heading\">Problem 2</div>\n",
    "<div class=\"panel-body\">\n",
    "Implement a transition-based dependency parser, train it on the specified training data, and evaluate its performance on the development data. Your parser should get the same results as the reference implementation.\n",
    "</div>\n",
    "</div>\n",
    "\n",
    "### Implementation\n",
    "\n",
    "Your starting point for this problem is the following skeleton class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Parser(nlp4.Parser):\n",
    "    \"\"\"A transition-based dependency parser.\n",
    "    \n",
    "    This parser implements the arc-standard algorithm for dependency\n",
    "    parsing. When being presented with an input sentence, it first\n",
    "    tags the sentence for parts of speech, and then uses a multi-class\n",
    "    perceptron classifier to predict a sequence of *moves*\n",
    "    (transitions) that construct a dependency tree for the input\n",
    "    sentence. Moves are encoded as integers as follows:\n",
    "    \n",
    "    SHIFT = 0, LEFT-ARC = 1, RIGHT-ARC = 2\n",
    "    \n",
    "    At any given point in the predicted sequence, the state of the\n",
    "    parser can be specified by: the index of the first word in the\n",
    "    input sentence that the parser has not yet started to process; a\n",
    "    stack holding the indices of those words that are currently being\n",
    "    processed; and a partial dependency tree, represented as a list of\n",
    "    indices such that `tree[i]` gives the index of the head (parent\n",
    "    node) of the word at position `i`, or 0 in case the corresponding\n",
    "    word has not yet been assigned a head.\n",
    "    \n",
    "    Attributes:\n",
    "        tagger: A part-of-speech tagger.\n",
    "        classifier: A multi-class perceptron classifier used to\n",
    "            predict the next move of the parser.\n",
    "    \"\"\"\n",
    "    \n",
    "    def __init__(self):\n",
    "        \"\"\"Initialises a new parser.\"\"\"\n",
    "        \n",
    "        self.tagger = Tagger()\n",
    "        self.perceptron = Perceptron()\n",
    "        super().__init__()\n",
    "    \n",
    "    def parse(self, words):\n",
    "        \"\"\"Parses a sentence.\n",
    "        \n",
    "        Args:\n",
    "            words: The input sentence, a list of words.\n",
    "        \n",
    "        Returns:\n",
    "            A pair consisting of the predicted tags and the predicted\n",
    "            dependency tree for the input sentence.\n",
    "        \"\"\"\n",
    "\n",
    "        tags = self.tagger.tag(words)\n",
    "        \n",
    "        i = 0\n",
    "        stack = []\n",
    "        pred_tree = [0] * len(words)\n",
    "        while True:\n",
    "            valid_moves = self.valid_moves(i, stack, pred_tree)\n",
    "            if not valid_moves:\n",
    "                break   \n",
    "            curr_features = self.features(words, tags, i, stack, pred_tree)\n",
    "            move_to_do = self.perceptron.predict(curr_features, valid_moves)\n",
    "            (i, stack, pred_tree) = self.move(i, stack, pred_tree, move_to_do)\n",
    "            \n",
    "        return (tags, pred_tree)\n",
    "\n",
    "    \n",
    "    def valid_moves(self, i, stack, pred_tree):\n",
    "        \"\"\"Returns the valid moves for the specified parser\n",
    "        configuration.\n",
    "        \n",
    "        Args:\n",
    "            i: The index of the first unprocessed word.\n",
    "            stack: The stack of words (represented by their indices)\n",
    "                that are currently being processed.\n",
    "            pred_tree: The partial dependency tree.\n",
    "        \n",
    "        Returns:\n",
    "            The list of valid moves for the specified parser\n",
    "                configuration.\n",
    "        \"\"\"\n",
    "        \n",
    "        valid = []\n",
    "        if i < len(words):\n",
    "            valid.append(0)\n",
    "        if len(stack) >= 2:\n",
    "            valid.append(2)\n",
    "        if len(stack) >= 3:\n",
    "            valid.append(1)\n",
    "        \n",
    "        return valid\n",
    "    \n",
    "    def move(self, i, stack, pred_tree, move):\n",
    "        \"\"\"Executes a single move.\n",
    "        \n",
    "        Args:\n",
    "            i: The index of the first unprocessed word.\n",
    "            stack: The stack of words (represented by their indices)\n",
    "                that are currently being processed.\n",
    "            pred_tree: The partial dependency tree.\n",
    "            move: The move that the parser should make.\n",
    "        \n",
    "        Returns:\n",
    "            The new parser configuration, represented as a triple\n",
    "            containing the index of the new first unprocessed word,\n",
    "            stack, and partial dependency tree.\n",
    "        \"\"\"\n",
    "        \n",
    "        if move == 0:\n",
    "            stack.append(i)\n",
    "            i += 1\n",
    "        elif move == 1:\n",
    "            pred_tree[stack[-2]] = stack[-1]\n",
    "            stack.remove(stack[-2])\n",
    "        elif move == 2:\n",
    "            pred_tree[stack[-1]] = stack[-2] \n",
    "            stack.remove(stack[-1])\n",
    "\n",
    "        return (i, stack, pred_tree)\n",
    "    \n",
    "    def update(self, words, gold_tags, gold_tree):\n",
    "        \"\"\"Updates the move classifier with a single training\n",
    "        instance.\n",
    "        \n",
    "        Args:\n",
    "            words: The input sentence, a list of words.\n",
    "            gold_tags: The list of gold-standard tags for the input\n",
    "                sentence.\n",
    "            gold_tree: The gold-standard tree for the sentence.\n",
    "        \n",
    "        Returns:\n",
    "            A pair consisting of the predicted tags and the predicted\n",
    "            dependency tree for the input sentence.\n",
    "        \"\"\"\n",
    "        \n",
    "        tags = self.tagger.update(words, gold_tags)\n",
    "        \n",
    "        i = 0\n",
    "        stack = []\n",
    "        pred_tree = [0] * len(words)\n",
    "        \n",
    "        while True:\n",
    "            move_to_do = self.gold_move(i, stack, pred_tree, gold_tree)\n",
    "            if move_to_do == None:\n",
    "                break\n",
    "            curr_features = self.features(words, gold_tags, i, stack, pred_tree)\n",
    "            self.perceptron.update(curr_features, move_to_do)\n",
    "            (i, stack, pred_tree) = self.move(i, stack, pred_tree, move_to_do)\n",
    "            \n",
    "        return (tags, pred_tree)\n",
    "    \n",
    "    def gold_move(self, i, stack, pred_tree, gold_tree):\n",
    "        \"\"\"Returns the gold-standard move for the specified parser\n",
    "        configuration.\n",
    "        \n",
    "        The gold-standard move is the first possible move from the\n",
    "        following list: LEFT-ARC, RIGHT-ARC, SHIFT. LEFT-ARC is\n",
    "        possible if the topmost word on the stack is the gold-standard\n",
    "        head of the second-topmost word, and all words that have the\n",
    "        second-topmost word on the stack as their gold-standard head\n",
    "        have already been assigned their head in the predicted tree.\n",
    "        Symmetric conditions apply to RIGHT-ARC. SHIFT is possible if\n",
    "        at least one word in the input sentence still requires\n",
    "        processing.\n",
    "        \n",
    "        Args:\n",
    "            i: The index of the first unprocessed word.\n",
    "            stack: The stack of words (represented by their indices)\n",
    "                that are currently being processed.\n",
    "            pred_tree: The partial dependency tree.\n",
    "            gold_tree: The gold-standard dependency tree.\n",
    "        \n",
    "        Returns:\n",
    "            The gold-standard move for the specified parser\n",
    "            configuration, or `None` if no move is possible.\n",
    "        \"\"\"\n",
    "        \n",
    "        if len(stack) < 3 and i < len(words):\n",
    "            return 0;\n",
    "        elif len(stack) <= 1 and i >= len(words):\n",
    "            return None\n",
    "        \n",
    "        elif stack[-1] == gold_tree[stack[-2]]:\n",
    "            left_arc = True;\n",
    "            for t in range(len(gold_tree)):\n",
    "                if gold_tree[t] == stack[-2]:\n",
    "                    if pred_tree[t] != stack[-2]:\n",
    "                        left_arc = False;\n",
    "            if left_arc:\n",
    "                return 1\n",
    "            \n",
    "        elif stack[-2] == gold_tree[stack[-1]]:\n",
    "            right_arc = True;\n",
    "            for t in range(len(gold_tree)):\n",
    "                if gold_tree[t] == stack[-1]:\n",
    "                    if pred_tree[t] != stack[-1]:\n",
    "                        right_arc = False;\n",
    "            if right_arc:\n",
    "                return 2\n",
    "            \n",
    "        return 0;\n",
    "\n",
    "    \n",
    "    def features(self, words, tags, i, stack, parse):\n",
    "        \"\"\"Extracts features for the specified parser configuration.\n",
    "\n",
    "        Args:\n",
    "            words: The input sentence, a list of words.\n",
    "            gold_tags: The list of gold-standard tags for the input\n",
    "                sentence.\n",
    "            i: The index of the first unprocessed word.\n",
    "            stack: The stack of words (represented by their indices)\n",
    "                that are currently being processed.\n",
    "            parse: The partial dependency tree.\n",
    "\n",
    "        Returns:\n",
    "            A feature vector for the specified configuration.\n",
    "        \"\"\"\n",
    "\n",
    "        \n",
    "        feats = []\n",
    "        if i < len(words):\n",
    "            feats.append(\"b[0]=\"+words[i])\n",
    "            feats.append(\"t[b[0]]=\"+tags[i])\n",
    "        if len(stack) > 0:\n",
    "            feats.append(\"s[-1]=\"+words[stack[-1]])\n",
    "            feats.append(\"t[s[-1]]=\"+tags[stack[-1]])\n",
    "        if len(stack) > 1:\n",
    "            feats.append(\"s[-2]=\"+words[stack[-2]])\n",
    "            feats.append(\"t[s[-2]]=\"+tags[stack[-2]])\n",
    "                                    \n",
    "        return feats\n",
    "        \n",
    "    \n",
    "    def finalize(self):\n",
    "        \"\"\"Averages the weight vectors.\"\"\"\n",
    "       \n",
    "        self.perceptron.finalize();\n",
    "        self.tagger.finalize();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Comments\n",
    "\n",
    "#### Main parsing method\n",
    "\n",
    "The first step in the parsing is to tag the sentence for parts of speech. After tagging, the `parse()` method initialises a new parser configuration and asks the move classifier to predict the move that the parser should make in that configuration. It is important that this prediction is restricted to predict a valid move; this is where the optional argument of the `predict()` method in the multi-class perceptron becomes relevant (see above). Making the predicted move results in a new configuration, for which the process is iterated; this continues as long as there are valid moves left to make.\n",
    "\n",
    "#### Extract features\n",
    "\n",
    "The reference implementation extracts the following features:\n",
    "\n",
    "* word form of the next word to be processed\n",
    "* tag of the next word to be processed\n",
    "* word form of the topmost word on the stack\n",
    "* tag of the topmost word on the stack\n",
    "* word form of the second-topmost word on the stack\n",
    "* tag of the second-topmost word on the stack\n",
    "\n",
    "In your own implementation, make sure to handle the special cases where there are no more words left that need to be processed, or the stack is empty."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluate the parser"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the following code to train and evaluate your parser. You can control the number of examples used for training by setting the value `n_examples`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 35,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Updated with sentence #12542\n",
      "Parsing sentence #2001\n",
      "Tagging accuracy: 86.97%\n",
      "Unlabelled attachment score: 61.05%\n"
     ]
    }
   ],
   "source": [
    "n_examples = None  # Set to None to train on all examples\n",
    "\n",
    "parser = Parser()\n",
    "with open(\"/home/TDDE09/labs/l4/data/en-ud-train-projective.conllu\") as fp:\n",
    "    for i, (words, gold_tags, gold_tree) in enumerate(trees(fp)):\n",
    "        parser.update(words, gold_tags, gold_tree)\n",
    "        print(\"\\rUpdated with sentence #{}\".format(i), end=\"\")\n",
    "        if n_examples and i >= n_examples:\n",
    "            break\n",
    "    print(\"\")\n",
    "parser.finalize()\n",
    "\n",
    "acc_k = acc_n = 0\n",
    "uas_k = uas_n = 0\n",
    "with open(\"/home/TDDE09/labs/l4/data/en-ud-dev.conllu\") as fp:\n",
    "    for i, (words, gold_tags, gold_tree) in enumerate(trees(fp)):\n",
    "        pred_tags, pred_tree = parser.parse(words)\n",
    "        acc_k += sum(int(g == p) for g, p in zip(gold_tags, pred_tags)) - 1\n",
    "        acc_n += len(words) - 1\n",
    "        uas_k += sum(int(g == p) for g, p in zip(gold_tree, pred_tree)) - 1\n",
    "        uas_n += len(words) - 1\n",
    "        print(\"\\rParsing sentence #{}\".format(i), end=\"\")\n",
    "    print(\"\")\n",
    "print(\"Tagging accuracy: {:.2%}\".format(acc_k / acc_n))\n",
    "print(\"Unlabelled attachment score: {:.2%}\".format(uas_k / uas_n))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With your own implementation you should obtain scores very similar to the ones of the reference implementation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.3"
  },
  "latex_envs": {
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 0
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

import bz2
import itertools
from itertools import islice
from scipy.sparse import csr_matrix
import scipy.sparse
import math
import numpy as np
from sklearn.decomposition import TruncatedSVD

def tokens(source):
    for sentence in source:
        for token in sentence.split():
            yield token

def make_vocab(source, threshold=100):

    vocabulary = {}
    for next_word in tokens(source):

        vocabulary[next_word] = vocabulary.get(next_word, 0) + 1
    v_with_threshold = {k: v for k, v in vocabulary.items() if v >= threshold}
    i = 0
    for word in v_with_threshold:
        v_with_threshold[word] = i
        i += 1

    return v_with_threshold


def contexts(source, k):
    # TODO: Replace the following line with your own code

    def window(it, k):
        l = list(itertools.islice(it, 2*k+1))

        while 1:
            yield tuple(l)
            l.append(next(it))
            l = l[1:]

    for sentence in source:

        def generate_sentence():
            for _ in range(k):
                yield b'<bos>'
            for token in sentence.split():
                yield token
            for _ in range(k):
                yield b'<eos>'

        yield from window(generate_sentence(), k)


def make_ppmi_matrix(vocab, source, k=2, delta=1.0):

    def format_context(context):
        only_context = context[0:k] + context[k+1:]
        return (list(only_context), context[k])

    
    word_count = {}
    context_count = {}
    word_context_count = {}
    N = 0
    for ctx in contexts(source,k):
        only_context, target = format_context(ctx)
        
        if target in vocab:
            
            for current_context in only_context:
                
                if current_context in vocab:
                    N += 1
                    word_count[target] = word_count.get(target, 0) + 1
                    context_count[current_context] = context_count.get(current_context, 0) + 1
                    # if target in vocab:
                    if target not in word_context_count:
                        word_context_count[target] = {}
                    word_context_count[target][current_context] = word_context_count[target].get(current_context, 0) + 1
    row = []
    col = []
    data = []

    for target in word_context_count:
        for context in word_context_count[target]:
            pmi = math.log((word_context_count[target][context]*N)/(word_count[target]*context_count[context]))
            ppmi = max(0, pmi - math.log(delta))
            if ppmi != 0:
                row.append(vocab[target])
                col.append(vocab[context])
                data.append(ppmi)

    return csr_matrix((data, (row, col)), shape=(max(row)+1, max(col)+1))


class Embedding(object):
    """A word embedding.

    A word embedding represents words as vectors in a d-dimensional
    vector space. The central attribute of the word embedding is a
    dense matrix whose rows correspond to the words in some
    vocabulary, and whose columns correspond to the d dimensions in
    the embedding space.

    Attributes:

        vocab: The vocabulary, specified as a dictionary that maps
            words to integer indices, identifying rows in the embedding
            matrix.
        dim: The dimensionality of the word vectors.
        m: The embedding matrix. The rows of this matrix correspond to the
            words in the vocabulary.
    """

    def __init__(self, vocab, matrix, dim=100):
        """Initialies a new word embedding.

        Args:
            vocab: The vocabulary for the embedding, specified as a
                dictionary that maps words to integer indices.
            matrix: The co-occurrence matrix, represented as a SciPy
                sparse matrix with one row and one column for each word in
                the vocabulary.
            dim: The dimensionality of the word vectors.
        """
        self.vocab = vocab
        self.dim = dim
        # TODO: Replace the following line with your own code
        svd = TruncatedSVD(n_components=dim)
        self.m  = svd.fit_transform(matrix)

        # self.m = np.zeros((len(vocab), dim))

    def vec(self, w):
        """Returns the vector for the specified word.

        Args:
            w: A word, an element of the vocabulary.

        Returns:
            The word vector for the specified word.
        """
        # TODO: Replace the following line with your own code
        return self.m[self.vocab[w]]
        #return np.zeros((1, self.dim))

    def similarity(self, w1, w2):
        """Computes the cosine similarity between the specified words.

        Args:
            w1: The first word (an element of the vocabulary).
            w2: The second word (an element of the vocabulary).

        Returns:
            The cosine similarity between the specified words in this
            embedding.
        """
        w1_vec = self.vec(w1)
        w2_vec = self.vec(w2)

        cosine_sim = np.dot(w1_vec, w2_vec) / (np.sqrt(w1_vec.dot(w1_vec))*np.sqrt(w2_vec.dot(w2_vec)))
        # TODO: Replace the following line with your own code
        return cosine_sim

#print("Creating small_vocab...")
#with bz2.open('simplewiki-small.txt.bz2') as source:
#    small_vocab = make_vocab(source)
#    print("Done.")
def create_all_data():
    print("Creating vocab...")
    with bz2.open('simplewiki.txt.bz2') as source:
        vocab = make_vocab(source)
        print("Done.")

#print("Creating PPMI matrix for small vocab...")
#with bz2.open('simplewdistance

    #print("Creating PPMI matrix for vocab...")
    #with bz2.open('simplewiki.txt.bz2') as source:
    #    ppmi_matrix = make_ppmi_matrix(vocab, source)
    #    print("Done")
    print("Opening PPMI...")
    #scipy.sparse.save_npz('simplewiki.npz', ppmi_matrix)
    ppmi_matrix = scipy.sparse.load_npz('simplewiki.npz')
    print("Done.")

    embedding = Embedding(vocab, ppmi_matrix)
    return embedding


from gensim.models import KeyedVectors

# word2vec : 3min20
# Lab5 vocab : 0.3125
# oanc vicab + word2vec : 0.575


#embedding = create_all_data()

# Lab5
# oanc
# wiki
import sys
if len(sys.argv) != 2:
    print("Usage : python lab5x.py [option]")
    print("With option one of the following : ")
    print("lab5 : use the word embedding from lab5 trainned on data simplewiki.txt.bz2")
    print("oanc : use the word embedding from gensim trainned on the vectors generated by word2vec on data oanc.txt")
    print("wiki : use the word embedding from gensim trainned on the vectors generated by word2vec on data enwik9.txt")
    sys.exit(-1)


arg = sys.argv[1]
embedding = None
if arg == "lab5":
    embedding = create_all_data()
elif arg == "oanc":
    embedding = KeyedVectors.load_word2vec_format('oanc_emb.bin', binary=True)
elif arg == "wiki":
    embedding = KeyedVectors.load_word2vec_format('enwik9_emb.bin', binary=True)
else:
    print("Argument not correct")
    sys.exit(-1)

print(embedding.similar_by_vector("sleep", topn=1, restrict_vocab=None))
print("Computing accuracy...")
with open("toefl.txt") as source:
    total = 0
    total_correct = 0
    for line in source:
        total += 1
        all_words = []
        for word in line.split():
            if arg == "lab5":
                all_words.append(word.encode('UTF-8'))
            else:
                all_words.append(word)
        original = all_words[0]
        choices = all_words[2:]
        synonym = choices[int(all_words[1])]
        max_similarity = float("-inf")
        max_choice = None
        for c in choices:
            try:
                new_similarity = embedding.similarity(c, original)
            except:
                new_similarity = -1
            if new_similarity > max_similarity:
                max_similarity = new_similarity
                max_choice = c
        if max_choice == synonym:
            total_correct += 1
    print("Done.\n")
    print("Accuracy = " + str(total_correct / total))
